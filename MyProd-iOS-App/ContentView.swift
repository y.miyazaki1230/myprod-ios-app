//
//  ContentView.swift
//  MyCamera
//
//  Created by 宮崎 祐介 on 2021/10/09.
//

import SwiftUI
import StoreKit

// アプリ内広告表示に必要
import GoogleMobileAds

// アプリ内広告表示に必要＠広告関連ライブラリの読み込み
struct AdBannerView: UIViewRepresentable {
    func makeUIView(context: Context) -> GADBannerView {
        let banner = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        // 開発　※Googleの指定
        // banner.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        // 本番　※Google AdMobで自分で作成した広告ユニットID
        banner.adUnitID = "ca-app-pub-6439802238937259/2934871678"
        banner.rootViewController = UIApplication.shared.windows.first?.rootViewController
        banner.load(GADRequest())
        return banner
    }
    
    func updateUIView(_ uiView: UIViewType, context: Context) {
    }
}

struct ContentView: View {
    
    // アプリ内課金のプロダクトID
    let MyAppBillingProductIdentifier : [String] = ["com.myzkysk.MyProd_iOS_App.myAutomaticRenewalSubscription1"]
    
    // アプリ内課金実装クラス（課金情報取得）
    // @ObservedObject var viewInfoModel: MyAppBillingInfoModel
    
    // アプリ内課金実装クラス（課金情報取得＋購入）
    @ObservedObject var viewModel: MyAppBillingCallModel
    
    init(viewModel:MyAppBillingCallModel) {
        // self.viewInfoModel = viewInfoModel
        self.viewModel = viewModel
    }
    
    // アプリ課金情報（未使用）
    @State var myAppBillingPriceLabel: String = ""
    
    // アプリ課金有無
    @State var isMyAppBilling = false
    
    // サンプルカメラアプリ表示フラグ
    @State var isCameraShowSheet = false
    
    // サンプル Lucky Number 表示アプリのナンバー
    @State var luckyNumber = Int.random(in: 000...999)
    
    var body: some View {
        
        VStack {
            
            Group {
                // Text("サンプルアプリの森")
                Text("アプリの森")
                    .font(.largeTitle)
                    .foregroundColor(Color.blue)
                    .multilineTextAlignment(.center)
                
                Text("＜iOSアプリ編＞")
                    .font(.title)
                    .foregroundColor(Color.blue)
                    .multilineTextAlignment(.center)
            }
            
            ScrollView {
                
                Group {
                    
                    Image("TreeOfMiyaza")
                        .resizable()
                        .scaledToFit()
                        // .frame(width: 300, height: 150)
                        .padding()
                    
                    // Text("iOSアプリ開発サンプルアプリ集")
                    Text("iOSアプリ開発アプリ集")
                        .font(.headline)
                        .foregroundColor(Color.blue)
                        .multilineTextAlignment(.center)
                    
                }
                
                /* アプリ内課金の審査が通らないため非表示
                 
                // アプリ内課金のための課金ボタン
                
                 Button(action: {
                    print("MyAppBillingProductIdentifier[0]")
                    viewModel.buy(product: MyAppBillingProductIdentifier[0])
                }) {
                    
                    // Text("サンプルアプリ表示\n（アプリ内課金¥100 / 1ヶ月の定期購読）" +
                    Text("アプリの実装URL取得\n（アプリ内課金¥100 / 1ヶ月の定期購読）" + myAppBillingPriceLabel)
                        .frame(maxWidth: .infinity)
                        .frame(height: 50)
                        .multilineTextAlignment(.center)
                        .background(Color.blue)
                        .foregroundColor(Color.white)
                }
                .padding()
                
                // アプリ内課金のための復元ボタン
                Button(action: {
                    viewModel.restore(product: MyAppBillingProductIdentifier[0])
                }) {
                    Text("アプリ内課金（定期購読）のリストア")
                        .frame(maxWidth: .infinity)
                        .frame(height: 50)
                        .multilineTextAlignment(.center)
                        .background(Color.blue)
                        .foregroundColor(Color.white)
                }
                .padding()
                
                
                // アプリ内課金済の場合に非表示
                if !isMyAppBilling && !viewModel.purchasedFlg {
                    // Text("「サンプルアプリ表示ボタン」で定期購読完了すると　\n　サンプルアプリを表示するためのボタンが活性化し、様々なサンプルアプリを操作可能！！")
                    Text("「アプリの実装URL取得」で定期購読完了すると\n　本アプリ自体の実装を書いたURLを表示\n→本アプリを体感しながら、本アプリそのものの実装を\n見ることで、iOSアプリ開発の勉強に")
                        .font(.footnote)
                        .foregroundColor(Color.blue)
                        .multilineTextAlignment(.center)
                        .lineLimit(nil)
                        .padding()
                }
                
                // アプリ内課金の審査が通らないため非表示 */
          
                // アプリ内課金済の場合に表示
                // iOSアプリの実装のURLを表示（GitLab）
                // if isMyAppBilling || viewModel.purchasedFlg {
                    
                 Text("経験ゼロの初心者のためのアプリ\n実物のアプリを動かしながら、\n実物の実装見て学ぶ、\nアプリ開発の勉強のためのアプリ\n")
                    .font(.headline)
                    .foregroundColor(Color.blue)
                    .multilineTextAlignment(.center)
                    .padding()
                
                    if let ios_app_impl_url = URL(string: "https://gitlab.com/y.miyazaki1230/myprod-ios-app") {
                        Link("iOSアプリ実装URL", destination: ios_app_impl_url)
                            .font(.title)
                    }
                    
                // }
                
                // アプリ内課金済の場合に表示
                // if isMyAppBilling || viewModel.purchasedFlg {
                    
                    // サンプルカメラアプリへ遷移するためのボタン
                    Button(action: {
                        isCameraShowSheet = true
                    }) {
                        // Text("サンプルカメラアプリ")
                        Text("カメラアプリ")
                            .frame(maxWidth: .infinity)
                            .frame(height: 50)
                            .multilineTextAlignment(.center)
                            .background(Color.blue)
                            .foregroundColor(Color.white)
                    }
                    .padding()
                    
                    // サンプルカメラアプリへ遷移するためのボタンを押下することでサンプルアプリ画面へ遷移
                    .sheet(isPresented: $isCameraShowSheet) {
                        CameraView(isCameraShowSheet: $isCameraShowSheet)
                    }
                    
                // }
                
                
                // Text("〜広告表示エリア〜")
                //    .font(.headline)
                //    .foregroundColor(Color.blue)
                //    .multilineTextAlignment(.center)
                //    .padding()
                
                // アプリ内広告表示に必要＠バナーの表示
                AdBannerView()
                
                // Text("〜広告表示エリア〜")
                //    .font(.headline)
                //    .foregroundColor(Color.blue)
                //    .multilineTextAlignment(.center)
                //    .padding()
                
                
                
                // アプリ内課金済の場合に表示
                // if isMyAppBilling || viewModel.purchasedFlg {
                    
                    Text("Lucky Number ： " + String(luckyNumber))
                        .font(.headline)
                        .foregroundColor(Color.blue)
                        .multilineTextAlignment(.center)
                    //.padding()
                    
                    Button(action: {
                        luckyNumber = Int.random(in: 000...999)
                    }) {
                        Text("Lucky Number 表示アプリ")
                            .frame(maxWidth: .infinity)
                            .frame(height: 50)
                            .multilineTextAlignment(.center)
                            .background(Color.blue)
                            .foregroundColor(Color.white)
                    }
                    .padding()
                
                // }
                
                // アプリ内課金有無切り替えボタン（通常非表示：デバッグ用）
                // Button(action: {
                //     isMyAppBilling.toggle()
                // }) {
                //     Text("サンプルカメラアプリ表示")
                //         .frame(maxWidth: .infinity)
                //         .frame(height: 50)
                //         .multilineTextAlignment(.center)
                //         .background(Color.blue)
                //         .foregroundColor(Color.white)
                // }
                // .padding()
                
                if let url_tou = URL(string: "https://s3.ap-northeast-1.amazonaws.com/ios.app.myzk/term-of-use.html") {
                    Link("利用規約URL", destination: url_tou)
                }
                
                if let url_pp = URL(string: "https://s3.ap-northeast-1.amazonaws.com/ios.app.myzk/privacy-policy.html") {
                    Link("プライバシーポリシーURL", destination: url_pp)
                }
                
            }
            
        }
        
        // 画面表示時アプリ内課金情報を取得
        .onAppear{
            
            if let appStoreReceiptURL = Bundle.main.appStoreReceiptURL,
               FileManager.default.fileExists(atPath: appStoreReceiptURL.path) {
                print(appStoreReceiptURL)
                
                // 端末にアプリ内課金レシート保有有≒アプリ内課金済　※厳密にはレシート検証でレシートの正当性の検証要
                isMyAppBilling = true
                
                // AppStore公開のためのApp ReviewでiPadのバグを指摘されたためコメントアウト
                //                do {
                //                    let receiptData = try Data(contentsOf: appStoreReceiptURL, options: .alwaysMapped)
                //                    let receiptString = receiptData.base64EncodedString(options: [])
                //                    print("receiptString: " + receiptString)
                //
                //                    // TODO: ココでレシート検証を実施
                //
                //                } catch {
                //                    print("Couldn't read receipt data with error: " + error.localizedDescription)
                //                }
            }
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView(
                viewModel: MyAppBillingCallModel .init(
                    download: MyAppBillingDownloadProduct.shared,
                    purchase: MyAppBillingPurchaseProduct.shared)
            )
            ContentView(
                viewModel: MyAppBillingCallModel .init(
                    download: MyAppBillingDownloadProduct.shared,
                    purchase: MyAppBillingPurchaseProduct.shared)
            )
        }
        
    }
    
}



