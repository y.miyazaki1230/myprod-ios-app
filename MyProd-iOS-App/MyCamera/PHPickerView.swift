//
//  PHPickerView.swift
//  MyCamera
//
//  Created by 宮崎 祐介 on 2021/10/10.
//

import SwiftUI
import PhotosUI

struct PHPickerView: UIViewControllerRepresentable {
    
    // Sheetが表示されているか
    @Binding var isShowSheet: Bool
    // フォトライブラリから読み込む写真
    @Binding var captureImage: UIImage?
    
    // Coordinator宣言
    class Coordinator: NSObject, PHPickerViewControllerDelegate {
        var parent: PHPickerView
        
        init(parent: PHPickerView) {
            self.parent = parent
        }
        
        // フォトライブラリで写真を選択・キャンセルしたときに実行されるdelegateメソッド
        func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
            
            // 写真は１つだけ選べる設定なので最初の１件を指定
            if let result = results.first {
                // UIImage型の写真のみ非同期で取得
                result.itemProvider.loadObject(ofClass: UIImage.self) {
                    (image, error) in
                    if let unwrapImage = image as? UIImage {
                        // 選択された写真を追加する
                        self.parent.captureImage = unwrapImage
                    } else {
                        print("使用できる写真がないです")
                    }
                }
                parent.isShowSheet = true
            } else {
                print("選択された写真がないです")
                parent.isShowSheet = false
            }
            // sheetを閉じる
//            parent.isShowSheet = false
        }
    }
    
    // Coordinatorを生成、SwiftUIによって自動的に呼び出し
    func makeCoordinator() -> Coordinator {
        Coordinator(parent: self)
    }
    
    // Viewを生成するときに実行
    func makeUIViewController(context: UIViewControllerRepresentableContext<PHPickerView>) -> PHPickerViewController {
        
        var configuration = PHPickerConfiguration()
        // 静止画を選択
        configuration.filter = .images
        // フォトライブラリで選択できる枚数を１枚にする
        configuration.selectionLimit = 1
        // PHPickerViewControllerのインスタンスを生成
        let picker = PHPickerViewController(configuration: configuration)
        // delegate設定
        picker.delegate = context.coordinator
        return picker
    }
    
    // Viewが更新されたときに実行
    func updateUIViewController(_ uiViewController: PHPickerViewController, context: UIViewControllerRepresentableContext<PHPickerView>) {
        //
    }
    
}

//struct PHPickerView_Previews: PreviewProvider {
//    static var previews: some View {
//        PHPickerView()
//    }
//}

struct PHPickerView_Previews: PreviewProvider {
    static var previews: some View {
        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
    }
}
