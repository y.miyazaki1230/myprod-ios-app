//
//  CameraView.swift
//  MyProd-iOS-App
//
//  Created by 宮崎 祐介 on 2022/04/02.
//

import SwiftUI

struct CameraView: View {
    
    // 前画面からの引き継ぎ
    @Binding var isCameraShowSheet: Bool
    
    // 撮影画像を保持する状態変数
    @State var captureImage: UIImage? = nil
    // 撮影画面のsheet
    @State var isShowSheet = false
    // シェア画面のsheet
    @State var isShowActivity = false
    // フォトライブラリかカメラかを保持する状態変数
    @State var isPhotoLibrary = false
    // ActionSheetのsheet
    @State var isShowAction = false
    
    var body: some View {
        
        VStack {
            
            Text("サンプルカメラアプリ")
                .font(.largeTitle)
                .foregroundColor(Color.blue)
                .multilineTextAlignment(.center)
                .padding()
            
            Text("①カメラ起動 / フォトライブラリ選択")
                .font(.title3)
                .foregroundColor(Color.blue)
                .multilineTextAlignment(.center)
                .padding()

            Text("②画像を選択")
                .font(.title3)
                .foregroundColor(Color.blue)
                .multilineTextAlignment(.center)
                .padding()

            Text("③エフェクトで画像加工 / シェア")
                .font(.title3)
                .foregroundColor(Color.blue)
                .multilineTextAlignment(.center)
                .padding()
            
            Text("④閉じるで元の画面")
                .font(.title3)
                .foregroundColor(Color.blue)
                .multilineTextAlignment(.center)
                .padding()

//            Spacer()
                
            Button(action: {
                captureImage = nil
                isShowAction = true
                }) {
                Text("カメラ起動 / フォトライブラリ選択")
                    .frame(maxWidth: .infinity)
                    .frame(height: 50)
                    .multilineTextAlignment(.center)
                    .background(Color.blue)
                    .foregroundColor(Color.white)
            }
            .padding()
            
            Button(action: {
                captureImage = nil
                isShowAction = false
                isShowSheet = false
                isCameraShowSheet = false
                }) {
                Text("閉じる")
                    .frame(maxWidth: .infinity)
                    .frame(height: 50)
                    .multilineTextAlignment(.center)
                    .background(Color.blue)
                    .foregroundColor(Color.white)
            }
            .padding()

            // 状態変数がtrueの時に ImagePickerView を起動
            // （例：カメラを起動する押下時）
            .sheet(isPresented: $isShowSheet) {
                
                if let unwrapCaptureImage = captureImage {
                    EffectView(isShowSheet: $isShowSheet, captureImage: unwrapCaptureImage)
                } else {
                
                    // フォトライブラリ
                    if isPhotoLibrary {
                        PHPickerView(isShowSheet: $isShowSheet, captureImage: $captureImage)
                    } else {
                        // 写真撮影
                        ImagePickerView(isShowSheet: $isShowSheet
                                        , captureImage: $captureImage)
                    }
                }
            }
            
            // 状態変数：$isShowActionに変化があったら実行
            .actionSheet(isPresented: $isShowAction) {
                // ActionSheetを表示
                ActionSheet(title: Text("画像の選択"),
                            message: Text("カメラ起動 / フォトライブラリ選択"),
                            buttons: [
                                .default(Text("カメラ"), action: {
                                    isPhotoLibrary = false
                                    // カメラ利用可能かチェック
                                    if UIImagePickerController.isSourceTypeAvailable(.camera) {
                                            print("カメラ利用可能")
                                            isShowSheet = true
                                    } else {
                                            print("カメラ利用不可")
                                    }
                                    
                                }),
                                .default(Text("フォトライブラリ"), action: {
                                    isPhotoLibrary = true
                                    isShowSheet = true
                                }),
                                .cancel(),
                            ])
            }
        
    }
        
    }
}

struct CameraView_Previews: PreviewProvider {
    static var previews: some View {
        CameraView(
            isCameraShowSheet: Binding.constant(true)
        )
        
    }
    
}
