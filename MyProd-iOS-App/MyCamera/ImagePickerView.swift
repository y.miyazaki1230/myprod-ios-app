//
//  ImagePickerView.swift
//  MyCamera
//
//  Created by 宮崎 祐介 on 2021/10/09.
//

import SwiftUI

struct ImagePickerView: UIViewControllerRepresentable {
       
    @Binding var isShowSheet: Bool
    @Binding var captureImage: UIImage?

    // Coodinatorを宣言
    class Coordinator: NSObject,
                       UINavigationControllerDelegate,
                       UIImagePickerControllerDelegate {
        let parent: ImagePickerView
        
        init(_ parent: ImagePickerView) {
            self.parent = parent
        }
        
        // カメラ撮影が終わったときに呼ばれる
        func imagePickerController(
            _ picker: UIImagePickerController,
            didFinishPickingMediaWithInfo info:
                [UIImagePickerController.InfoKey : Any]){
            
            // 撮影した写真をcaptureImageに保存
            if let originalImage =
                info[UIImagePickerController.InfoKey.originalImage]
                    as? UIImage {
                parent.captureImage = originalImage
            }
            
//            parent.isShowSheet = false
            parent.isShowSheet = true
        }
        
        // キャンセルボタンを押下したときに呼ばれる
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            parent.isShowSheet = false
        }
        
    }
    
    // SwifUIによって自動的に呼び出し
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

    // 表示するViewを作成するときに実行
    func makeUIViewController(context: UIViewControllerRepresentableContext<ImagePickerView>) -> UIImagePickerController {

        // インスタンス生成
        let myImagePickerController = UIImagePickerController()
        // 撮影画面のオプション カメラから写真を取得
        myImagePickerController.sourceType = .camera
        // delegate設定 通知先を指定
        myImagePickerController.delegate = context.coordinator
        return myImagePickerController
    }
    
    func updateUIViewController(_ uiViewController: UIImagePickerController, context: UIViewControllerRepresentableContext<ImagePickerView>) {
        //
    }
    
}

//struct ImagePickerView_Previews: PreviewProvider {
//    static var previews: some View {
//        ImagePickerView()
//    }
//}
