//
//  EffectView.swift
//  MyCamera
//
//  Created by 宮崎 祐介 on 2021/10/10.
//

import SwiftUI

let filterArray = ["CIPhotoEffectMono",
                   "CIPhotoEffectChrome",
                   "CIPhotoEffectFade",
                   "CIPhotoEffectInstant",
                   "CIPhotoEffectNoir",
                   "CIPhotoEffectProcess",
                   "CIPhotoEffectTonal",
                   "CIPhotoEffectTransfer",
                   "CISepiaTone"
]

var filterSelectNumber = 0

struct EffectView: View {

    @Binding var isShowSheet: Bool
    let captureImage: UIImage
    @State var showImage: UIImage?
    @State var isShowActivity = false

    var body: some View {
        VStack {
            Spacer()
            
            if let unwrapShowImage = showImage {
                Image(uiImage: unwrapShowImage)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
            }
            
            Spacer()
            
            Button(action: {
                // フィルタ名を指定
//                let filterName = "CIPhotoEffectMono"
                let filterName = filterArray[filterSelectNumber]
                filterSelectNumber += 1
                if filterSelectNumber == filterArray.count {
                    filterSelectNumber = 0
                }
                // 元々の画像の回転角度取得
                let rotate = captureImage.imageOrientation
                // UIImage形式の画像をCIImage形式に変換
                let inputImage = CIImage(image: captureImage)

                // CIFilterのインスタンス生成
                guard let effectFilter = CIFilter(name: filterName) else {return}
                // Filter加工のパラメータ初期化
                effectFilter.setDefaults()
                // Filter加工する元画像設定
                effectFilter.setValue(inputImage, forKey: kCIInputImageKey)
                // Filter加工する情報を生成
                guard let outputImage = effectFilter.outputImage else {return}

                // CIContextのインスタンス生成
                let ciContext = CIContext(options: nil)
                // 画像の加工生成
                guard let cgImage = ciContext.createCGImage(outputImage, from: outputImage.extent) else {return}
                // CGImage形式からUIImage形式へ変換
                showImage = UIImage(cgImage: cgImage, scale: 1.0, orientation: rotate)

            }) {
                Text("エフェクト")
                    .frame(maxWidth: .infinity)
                    .frame(height: 50)
                    .multilineTextAlignment(.center)
                    .background(Color.blue)
                    .foregroundColor(.white)
            }
            .padding()

            Button(action: {
                isShowActivity = true
            }) {
                Text("シェア")
                    .frame(maxWidth: .infinity)
                    .frame(height: 50)
                    .multilineTextAlignment(.center)
                    .background(Color.blue)
                    .foregroundColor(.white)
            }
            .sheet(isPresented: $isShowActivity) {
                // シェア画面の表示
                ActivityView(shareItems: [showImage!.resize()!])
            }
            .padding()

            Button(action: {
                isShowSheet = false
            }) {
                Text("閉じる")
                    .frame(maxWidth: .infinity)
                    .frame(height: 50)
                    .multilineTextAlignment(.center)
                    .background(Color.blue)
                    .foregroundColor(.white)
            }
            .padding()
        }

        // 写真が表示されるときに最初の一度だけ実行される
        .onAppear {
            showImage = captureImage
        }

    }
}

struct EffectView_Previews: PreviewProvider {
    static var previews: some View {
        EffectView(
            isShowSheet: Binding.constant(true),
            captureImage: UIImage(named: "preview_use")!
        )
    }
}
