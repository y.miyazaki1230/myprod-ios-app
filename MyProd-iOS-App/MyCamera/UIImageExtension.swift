//
//  UIImageExtension.swift
//  MyCamera
//
//  Created by 宮崎 祐介 on 2021/10/24.
//

import Foundation
import UIKit

// 画像のリサイズ処理
extension UIImage {
    func resize() -> UIImage? {
        let rate = 1024.0 / self.size.width
        let rect = CGRect(x:0, y:0, width: self.size.width * rate, height: self.size.height * rate)
        
        UIGraphicsBeginImageContext(rect.size)
        self.draw(in: rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
}
