//
//  MyProd-iOS-App.swift
//  MyProd-iOS
//
//  Created by 宮崎 祐介 on 2021/10/09.
//

import SwiftUI
// プッシュ通知に必要
import UserNotifications
// アプリ内課金に必要
import StoreKit
// アプリ内広告表示に必要
import GoogleMobileAds

@main
struct MyProdiOSApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    var body: some Scene {
        WindowGroup {
                     
            ContentView(
                viewModel: .init(
                    download: MyAppBillingDownloadProduct.shared,
                    purchase: MyAppBillingPurchaseProduct.shared)
            )
            
        }
    }
}

class AppDelegate: UIResponder, UIApplicationDelegate {
        
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        // （アラート、サウンド、バッジを使用した）プッシュ通知許可
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            guard granted else { return }
            
            // APNsに対してデバイストークンの要求（どのデバイスのどのアプリかをAPNsに登録）
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
        
        // iOSアプリ内課金トランザクションの監視処理の登録開始
        SKPaymentQueue.default().add(MyAppBillingPurchaseProduct.shared)
        
        // アプリ内広告表示に必要
        // Mobile Ads SDK を初期化し、初期化が完了（または 30 秒間のタイムアウトが経過）した時点で完了ハンドラをコールバック
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        
        return true
    }
}

// APNs登録後のコールバックを実装
extension AppDelegate {

    // デバイストークンの取得
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {

        let tokenBytes = deviceToken.map { (byte: UInt8) in String(format: "%02.2hhx", byte) }
        print("DeviceToken: \(tokenBytes.joined())")
    }
    
    // エラーハンドリング
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to Register to APNs: \(error)")
    }
}

struct MyProdiOSApp_Previews: PreviewProvider {
    static var previews: some View {
        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
    }
}
