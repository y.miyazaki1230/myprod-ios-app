//
//  MyPurchaseProduct.swift
//  MyProd-iOS-App
//
//  Created by 宮崎 祐介 on 2022/01/30.
//

import Foundation
import StoreKit

protocol PurchasedResultNotification: AnyObject {
    
    func completed(transaction: SKPaymentTransaction)
    
    func failed(transaction: SKPaymentTransaction)
    
//    //リストアが完了した時
//    func restored(_ productIdentifiers: [String])
//
//    //リストアに失敗した時
//    func failedRestore()
//
//    //1度もアイテム購入したことがなく、リストアを実行した時
//    func failedRestoreNeverPurchased()
//
//    //特殊な購入時の延期の時
//    func deferredPurchased()
}

// 課金アイテムを購入する責務を持つクラス
final class MyAppBillingPurchaseProduct: NSObject {

    fileprivate var isRestoring = false
    fileprivate let paymentQueue = SKPaymentQueue.default()

    private override init() {}
    static let shared = MyAppBillingPurchaseProduct()
    
    weak var delegate: PurchasedResultNotification?
    
    func callAsFunction(product: SKProduct) {
        let payment = SKPayment(product: product)
        // キューに追加によって課金アイテム購入のためのトランザクション生成
        SKPaymentQueue.default().add(payment)
    }
    
    func restore(product id: String) {
        guard !isRestoring else { print("リストア処理中"); return }
        self.isRestoring = true
        paymentQueue.restoreCompletedTransactions()
    }
    
}

extension MyAppBillingPurchaseProduct: SKPaymentTransactionObserver {
    // トランザクション（購入処理）の状態通知
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        transactions.forEach { transaction in
            switch transaction.transactionState {
            case .purchased:
                print("Complete（アプリ内課金購入完了）!!")
                // ここのタイミングでレシートデータの取得＆レシート検証を実装する（割愛）
//                SKPaymentQueue.default().finishTransaction(transaction)
                delegate?.completed(transaction: transaction)
            case .restored:
                print("Complete（アプリ内課金リストア完了）!!")
                // ここのタイミングでレシートデータの取得＆レシート検証を実装する（割愛）
//                SKPaymentQueue.default().finishTransaction(transaction)
                delegate?.completed(transaction: transaction)
            case .failed:
                print("Failed（アプリ内課金購入失敗）!!")
                // ここのタイミングでレシートデータの取得＆レシート検証を実装する（割愛）
//                SKPaymentQueue.default().finishTransaction(transaction)
                delegate?.failed(transaction: transaction)
            case .purchasing, .deferred:
                print ("Purchasing or Deffered（アプリ内課金購入処理中）...")
            @unknown default:
                break
            }
        }
    }
    
    //リストアの問い合わせが完了
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        self.isRestoring = false

//        guard !queue.transactions.isEmpty else {
//            delegate?.failedRestoreNeverPurchased()
//            return
//        }
//        self.isRestoring = false

        let productIdentifiers: [String] = {
            var identifiers: [String] = []
            queue.transactions.forEach {
                identifiers.append($0.payment.productIdentifier)
            }
            return identifiers
        }()

//        delegate?.restored(productIdentifiers)
    }
    
//    リストアの問い合わせが失敗
//    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
//        print("-----restoreFailed-----")
//        self.isRestoring = false
//        delegate?.failedRestore()
//    }
}












