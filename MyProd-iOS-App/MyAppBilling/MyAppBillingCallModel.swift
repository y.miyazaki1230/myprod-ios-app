//
//  MyAppBillingCallModel.swift
//  MyProd-iOS-App
//
//  Created by 宮崎 祐介 on 2022/01/30.
//

import Foundation
import StoreKit

final class MyAppBillingCallModel: ObservableObject {
    private let download: MyAppBillingDownloadProduct
    private let purchase: MyAppBillingPurchaseProduct
    
    // アプリ内課金成功時にtrueとなりViewに連携
    @Published var purchasedFlg = false
    
    fileprivate var isRestoring = false
    fileprivate let paymentQueue = SKPaymentQueue.default()

    init(download: MyAppBillingDownloadProduct, purchase: MyAppBillingPurchaseProduct) {
        self.download = download
        self.purchase = purchase
    }
    
//    func get(product id: String) -> String {
//
//        var myAppBillingPriceLabel: String = ""
//
//        download(productIds: [id], completion: {[weak self] (products : [SKProduct]?, error: NSError?) -> Void in
//                        
//            if error != nil {
//                if self != nil {
//                }
//                print(error?.localizedDescription)
//                return
//            }
//
//            for product in products! {
//
//                let numberFomatter = NumberFormatter()
//                numberFomatter.formatterBehavior = .behavior10_4
//                numberFomatter.numberStyle = .currency
//                numberFomatter.locale = product.priceLocale
//                let myAppBillingPriceString = numberFomatter.string(from: product.price)
//
//                myAppBillingPriceLabel = product.localizedTitle + ":\(String(describing: myAppBillingPriceString))"
//                print("pricelabel:" + myAppBillingPriceLabel)
//            }
//
//        })
//        return myAppBillingPriceLabel
//    }
    
    func buy(product id: String) {
        print("Transaction Start")
        download.delegate = self
        download(productIds: [id])
    }
    
    func restore(product id: String) {
        print("Restore Start")
        purchase.delegate = self
        purchase.restore(product: id)
    }
    
}
    
extension MyAppBillingCallModel: DownloadedProductNotification {
    
    func downloaded(products: [SKProduct]?, error: Error?) {
        download.delegate = nil
        guard let products = products, let product = products.first else {
            return
        }
        print("Purchase Start")
        purchase.delegate = self
        purchase(product: product)
    }
}

extension MyAppBillingCallModel: PurchasedResultNotification {
    
//    func restored(_ productIdentifiers: [String]) {
//    }
//    
//    func failedRestore() {
//    }
//    
//    func failedRestoreNeverPurchased() {
//    }
//    
//    func deferredPurchased() {
//    }
    
    func completed(transaction: SKPaymentTransaction) {
        self.purchase.delegate = nil
        SKPaymentQueue.default().finishTransaction(transaction)
        
        purchasedFlg = true
        
        print("Purchase End")
        print("Transaction End")
    }
    
    func failed(transaction: SKPaymentTransaction) {
        self.purchase.delegate = nil
        SKPaymentQueue.default().finishTransaction(transaction)
    }
}


