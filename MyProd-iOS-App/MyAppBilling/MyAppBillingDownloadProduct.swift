//
//  MyDownloadProduct.swift
//  MyProd-iOS-App
//
//  Created by 宮崎 祐介 on 2022/01/30.
//

import Foundation
import StoreKit

enum DownloadProductError: Error {
    case noProduct
    case invalidProduct
    
    var message: String {
        switch self {
            case .noProduct: return "No items available for purchase. Make sure the product ID is correct."
            case .invalidProduct: return "There is an invalid product."
        }
    }
}

protocol DownloadedProductNotification: AnyObject {
    func downloaded(products: [SKProduct]?, error: Error?)
}

// 課金アイテム情報を取得する責務を持つクラス
final class MyAppBillingDownloadProduct: NSObject {
    static let shared = MyAppBillingDownloadProduct()
        private override init() {}
        private var productsRequest: SKProductsRequest?
        weak var delegate: DownloadedProductNotification?
    
    func callAsFunction(productIds: [String]) {
        productsRequest = SKProductsRequest(productIdentifiers: Set(productIds))
        productsRequest?.delegate = self
        // 課金アイテム情報取得開始
        productsRequest?.start()
        print("Download Start")
    }
    
//    private var completionForProductidentifiers : (([SKProduct]?,NSError?) -> Void)?
    
//    func callAsFunction(productIds: [String], completion:(([SKProduct]?, NSError?) -> Void)?) {
//        productsRequest = SKProductsRequest(productIdentifiers: Set(productIds))
//        // productsRequest?.delegate = self
//        // 課金アイテム情報取得開始
//        productsRequest?.start()
//        print("Download Get Start")
//        MyAppBillingDownloadProduct().completionForProductidentifiers = completion
//    }
    
}

extension MyAppBillingDownloadProduct: SKProductsRequestDelegate {
    // 課金アイテムの取得結果を受け取る
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        // AppStoreConnectで正しい課金アイテム情報を登録できていないとempty
        guard !response.products.isEmpty else {
            delegate?.downloaded(products: nil, error: DownloadProductError.noProduct)
            return
        }
        // 不正な課金アイテムのチェック
        guard response.invalidProductIdentifiers.isEmpty else {
            delegate?.downloaded(products: nil, error: DownloadProductError.invalidProduct)
            return
        }
        // 課金アイテム取得正常終了
        print(response.products)
        print("Download End")
        delegate?.downloaded(products: response.products, error: nil)
    }
}










